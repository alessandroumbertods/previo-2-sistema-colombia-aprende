/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author madar
 */
public class Notificacion {

    private String descripcion;

    public Notificacion() {
    }

    public Notificacion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    /*
    public Notificacion(String nombre, int cedula, byte cantInfantes, byte cantAdolencentes) {
        
    }*/

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return descripcion;
    }

    
    
}
