/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Modelo.*;
import java.time.LocalTime;
import java.util.Iterator;
import ufps.util.colecciones_seed.Cola;
import ufps.util.colecciones_seed.ColaP;
import ufps.util.colecciones_seed.ListaCD;
import ufps.util.colecciones_seed.Pila;
import ufps.util.varios.ArchivoLeerURL;

/**
 *
 * @author madar
 */
public class Sistema_ColombiaAprende {

    private ListaCD<Notificacion> notificaciones;
    private Pila<Hardware_Academico>[] inventario;
    private ColaP<Usuario> usuarios;

    public Sistema_ColombiaAprende() {
        this.usuarios = new ColaP();
        this.inventario = new Pila[2];
        this.inventario[0] = new Pila(); //PC
        this.inventario[1] = new Pila(); //tablet
        this.notificaciones = new ListaCD();
    }

    public void cargarUsuarios(String url) {

        ArchivoLeerURL file = new ArchivoLeerURL(url);
        Object v[] = file.leerArchivo();
        for (int i = 1; i < v.length; i++) {
            String datos = v[i].toString();
            //CEDULA;Nombres;Correo;HORA DE REALIZACION DE REGISTRO(HH:MM:SS);cantidad_hijos_infantes;cantidad_hijos_adolecentes
            String datos2[] = datos.split(";");
            int cedula = Integer.parseInt(datos2[0]);
            String nombre = datos2[1];
            String email = datos2[2];
            LocalTime fecha = getFecha(datos2[3]);
            byte cI = Byte.parseByte(datos2[4]);
            byte cA = Byte.parseByte(datos2[5]);
            Usuario nuevo = new Usuario(cedula, nombre, email, fecha, cI, cA);
            this.usuarios.enColar(nuevo, nuevo.getPrioridad() * -1);
        }

        // :)
    }

    private LocalTime getFecha(String fecha) {
        String fecha2[] = fecha.split(":");
        return LocalTime.of(Integer.parseInt(fecha2[0]), Integer.parseInt(fecha2[1]), Integer.parseInt(fecha2[2]));

    }

    public void cargarHardware_Academico(String url) {

        ArchivoLeerURL file = new ArchivoLeerURL(url);
        Object v[] = file.leerArchivo();
        for (int i = 1; i < v.length; i++) {
            //id_hardware;tipo;descripcion
            String datos = v[i].toString();
            String datos2[] = datos.split(";");
            byte id = Byte.parseByte(datos2[0]);
            boolean esPC = Boolean.parseBoolean(datos2[1]);
            String descripcion = datos2[2];
            Hardware_Academico nuevo = new Hardware_Academico(esPC, id, descripcion);
            int i2 = esPC ? 0 : 1;
            this.inventario[i2].apilar(nuevo);

        }
        // :)
    }

    public void procesar_Entregas_Hardware() {
        ColaP<Usuario> respaldoUsuarios = new ColaP();
        Pila<Hardware_Academico> respaldoComputadoras = new Pila();
        Pila<Hardware_Academico> respaldoTablets = new Pila();
        while (!this.usuarios.esVacia()) {
            Usuario user = this.usuarios.deColar();
            respaldoUsuarios.enColar(user, user.getPrioridad()*-1);
            byte infante = user.getCantInfantes();
            byte adolescente = user.getCantAdolencentes();
            
            byte entregarInfante = entregarHardware(this.inventario[1], respaldoTablets, infante, false);
            byte entregarAdolescente = entregarHardware(this.inventario[0], respaldoComputadoras, adolescente, true);
            //byte entregarInfante = entregarHardware(this.inventario[1], infante, false);
            //byte entregarAdolescente = entregarHardware(this.inventario[0], adolescente, true);
            
            Notificacion notificacion = new Notificacion(mensajeDeNotificacion(user, entregarInfante, entregarAdolescente, infante, adolescente));
            this.notificaciones.insertarAlFinal(notificacion);
        }
        this.usuarios = respaldoUsuarios;
        this.inventario[0] = respaldoComputadoras;
        this.inventario[1] = respaldoTablets;
        //:)
    }

    public byte entregarHardware(Pila<Hardware_Academico> p, Pila<Hardware_Academico> respaldo,byte hijos, boolean computadores) {
        if (p.esVacia()) {
            return 0;
        }
        byte cantidad = hijos;
        if (computadores) {
            cantidad = (byte) Math.ceilDiv(hijos, 2);
        }
        while (!p.esVacia() && cantidad > 0) {
            try {
                Hardware_Academico hardware = p.desapilar();
                respaldo.apilar(hardware);
                cantidad--;
            } catch (Exception e) {
                return cantidad;
            }
        }
        if (computadores) {
            return (byte) Math.ceilDiv(hijos, 2);
        }
        return hijos;
    }

    public String getListadoUsuarios() {
        String msg = "";
        ColaP<Usuario> respaldo = new ColaP();
        while (!this.usuarios.esVacia()) {
            Usuario x = this.usuarios.deColar();
            respaldo.enColar(x, x.getPrioridad());
            msg += x.toString() + "\n";
        }
        this.usuarios = respaldo;
        return msg;
    }

    public String getListadoHardware() {
        return this.getListadoPila(this.inventario[0], 0) + this.getListadoPila(this.inventario[1], 1);
    }

    /**
     * HAY QUE ARRGLARLO PRO QUE LA PILA SE PIERDE
     *
     * @param p
     * @return
     */
    private String getListadoPila(Pila<Hardware_Academico> p, int i) {
        Pila<Hardware_Academico> pRespaldo = new Pila();
        String msg = "";
        while (!p.esVacia()) {
            Hardware_Academico hardware = p.desapilar();
            pRespaldo.apilar(hardware);
            msg += hardware.toString() + "\n";
        }
        this.inventario[i] = pRespaldo;
        return msg + "\n\n";
    }

    public String getListadoNotificaciones() {
        String msg = "";
        for (Notificacion info : this.notificaciones) {
            msg += info.toString() + "\n";
        }
        return msg;
    }

    public String mensajeDeNotificacion(Usuario user, byte entregarInfante, byte entregarAdolescente, byte infante, byte adolescente) {
        int cantidadComputadoras = (adolescente == 0) ? 0 : Math.ceilDiv(user.getCantAdolencentes(), 2);
        String mensaje = "El usuario " + user.getNombre() + " con cedula " + user.getCedula();
        if (entregarInfante == 0 && entregarAdolescente == 0) {
            mensaje += "NO SE LE ASIGNARON NI COMPUTADORES NI TABLETS, INVENTARIO VACÍO.";
        } else {
            mensaje += " se asigno " + entregarAdolescente + " computadores y/o " + entregarInfante + " tablets ";
            if (cantidadComputadoras - entregarAdolescente != 0 || infante - entregarInfante != 0) {
                mensaje += ", PERO le hicieron falta " + (entregarAdolescente - cantidadComputadoras) + " computadores y/o " + (infante - entregarInfante) + " tablets. ";
            }
        }
        return mensaje;
    }

}
