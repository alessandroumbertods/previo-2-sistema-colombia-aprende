/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.vistas;

import Negocio.Sistema_ColombiaAprende;

/**
 *
 * @author madar
 */
public class Prueba_previo2 {

    public static void main(String[] args) {

        String urlUsuario = "https://gitlab.com/pruebas_madarme/persistencia/ed_a/colombia_aprende/-/raw/main/personas_computadores.csv";
        Sistema_ColombiaAprende sistema = new Sistema_ColombiaAprende();
        sistema.cargarUsuarios(urlUsuario);
        System.out.println(sistema.getListadoUsuarios());
        String urlHw = "https://gitlab.com/pruebas_madarme/persistencia/ed_a/colombia_aprende/-/raw/main/hardware.csv";
        sistema.cargarHardware_Academico(urlHw);
        System.out.println(sistema.getListadoHardware());
        sistema.procesar_Entregas_Hardware();
        System.out.println(sistema.getListadoNotificaciones());

        /*
        System.out.println("LO REPITO PARA VER QUE NO SE PIERDAN LOS DATOS");
        System.out.println(sistema.getListadoUsuarios());
        System.out.println(sistema.getListadoHardware());
        System.out.println(sistema.getListadoNotificaciones());
         */
    }
}
